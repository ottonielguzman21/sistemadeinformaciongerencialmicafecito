package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * People generated by hbm2java
 */
@Entity
@Table(name="people"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="NAME") 
)
public class People  implements java.io.Serializable {


     private String id;
     private Roles roles;
     private String name;
     private String apppassword;
     private String card;
     private boolean visible;
     private byte[] image;
     private Set<Tickets> ticketses = new HashSet<Tickets>(0);

    public People() {
    }

	
    public People(String id, Roles roles, String name, boolean visible) {
        this.id = id;
        this.roles = roles;
        this.name = name;
        this.visible = visible;
    }
    public People(String id, Roles roles, String name, String apppassword, String card, boolean visible, byte[] image, Set<Tickets> ticketses) {
       this.id = id;
       this.roles = roles;
       this.name = name;
       this.apppassword = apppassword;
       this.card = card;
       this.visible = visible;
       this.image = image;
       this.ticketses = ticketses;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ROLE", nullable=false)
    public Roles getRoles() {
        return this.roles;
    }
    
    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    
    @Column(name="NAME", unique=true, nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="APPPASSWORD")
    public String getApppassword() {
        return this.apppassword;
    }
    
    public void setApppassword(String apppassword) {
        this.apppassword = apppassword;
    }

    
    @Column(name="CARD")
    public String getCard() {
        return this.card;
    }
    
    public void setCard(String card) {
        this.card = card;
    }

    
    @Column(name="VISIBLE", nullable=false)
    public boolean isVisible() {
        return this.visible;
    }
    
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    
    @Column(name="IMAGE")
    public byte[] getImage() {
        return this.image;
    }
    
    public void setImage(byte[] image) {
        this.image = image;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="people")
    public Set<Tickets> getTicketses() {
        return this.ticketses;
    }
    
    public void setTicketses(Set<Tickets> ticketses) {
        this.ticketses = ticketses;
    }




}


