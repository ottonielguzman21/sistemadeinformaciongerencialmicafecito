package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * ReservationCustomers generated by hbm2java
 */
@Entity
@Table(name="reservation_customers"
    ,catalog="openbravopos"
)
public class ReservationCustomers  implements java.io.Serializable {


     private String id;
     private Customers customers;
     private Reservations reservations;

    public ReservationCustomers() {
    }

    public ReservationCustomers(Customers customers, Reservations reservations) {
       this.customers = customers;
       this.reservations = reservations;
    }
   
     @GenericGenerator(name="generator", strategy="foreign", parameters=@Parameter(name="property", value="reservations"))@Id @GeneratedValue(generator="generator")

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="CUSTOMER", nullable=false)
    public Customers getCustomers() {
        return this.customers;
    }
    
    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

@OneToOne(fetch=FetchType.LAZY)@PrimaryKeyJoinColumn
    public Reservations getReservations() {
        return this.reservations;
    }
    
    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }




}


