package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Receipts generated by hbm2java
 */
@Entity
@Table(name="receipts"
    ,catalog="openbravopos"
)
public class Receipts  implements java.io.Serializable {


     private String id;
     private Closedcash closedcash;
     private Date datenew;
     private byte[] attributes;
     private Set<Payments> paymentses = new HashSet<Payments>(0);
     private Tickets tickets;
     private Set<Taxlines> taxlineses = new HashSet<Taxlines>(0);

    public Receipts() {
    }

	
    public Receipts(String id, Closedcash closedcash, Date datenew) {
        this.id = id;
        this.closedcash = closedcash;
        this.datenew = datenew;
    }
    public Receipts(String id, Closedcash closedcash, Date datenew, byte[] attributes, Set<Payments> paymentses, Tickets tickets, Set<Taxlines> taxlineses) {
       this.id = id;
       this.closedcash = closedcash;
       this.datenew = datenew;
       this.attributes = attributes;
       this.paymentses = paymentses;
       this.tickets = tickets;
       this.taxlineses = taxlineses;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="MONEY", nullable=false)
    public Closedcash getClosedcash() {
        return this.closedcash;
    }
    
    public void setClosedcash(Closedcash closedcash) {
        this.closedcash = closedcash;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATENEW", nullable=false, length=19)
    public Date getDatenew() {
        return this.datenew;
    }
    
    public void setDatenew(Date datenew) {
        this.datenew = datenew;
    }

    
    @Column(name="ATTRIBUTES")
    public byte[] getAttributes() {
        return this.attributes;
    }
    
    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="receipts")
    public Set<Payments> getPaymentses() {
        return this.paymentses;
    }
    
    public void setPaymentses(Set<Payments> paymentses) {
        this.paymentses = paymentses;
    }

@OneToOne(fetch=FetchType.LAZY, mappedBy="receipts")
    public Tickets getTickets() {
        return this.tickets;
    }
    
    public void setTickets(Tickets tickets) {
        this.tickets = tickets;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="receipts")
    public Set<Taxlines> getTaxlineses() {
        return this.taxlineses;
    }
    
    public void setTaxlineses(Set<Taxlines> taxlineses) {
        this.taxlineses = taxlineses;
    }




}


