package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Taxcategories generated by hbm2java
 */
@Entity
@Table(name="taxcategories"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="NAME") 
)
public class Taxcategories  implements java.io.Serializable {


     private String id;
     private String name;
     private Set<Taxes> taxeses = new HashSet<Taxes>(0);
     private Set<Products> productses = new HashSet<Products>(0);

    public Taxcategories() {
    }

	
    public Taxcategories(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public Taxcategories(String id, String name, Set<Taxes> taxeses, Set<Products> productses) {
       this.id = id;
       this.name = name;
       this.taxeses = taxeses;
       this.productses = productses;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    
    @Column(name="NAME", unique=true, nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="taxcategories")
    public Set<Taxes> getTaxeses() {
        return this.taxeses;
    }
    
    public void setTaxeses(Set<Taxes> taxeses) {
        this.taxeses = taxeses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="taxcategories")
    public Set<Products> getProductses() {
        return this.productses;
    }
    
    public void setProductses(Set<Products> productses) {
        this.productses = productses;
    }




}


