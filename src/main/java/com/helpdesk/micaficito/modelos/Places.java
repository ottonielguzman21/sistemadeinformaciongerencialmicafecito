package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Places generated by hbm2java
 */
@Entity
@Table(name="places"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="NAME") 
)
public class Places  implements java.io.Serializable {


     private String id;
     private Floors floors;
     private String name;
     private int x;
     private int y;

    public Places() {
    }

    public Places(String id, Floors floors, String name, int x, int y) {
       this.id = id;
       this.floors = floors;
       this.name = name;
       this.x = x;
       this.y = y;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="FLOOR", nullable=false)
    public Floors getFloors() {
        return this.floors;
    }
    
    public void setFloors(Floors floors) {
        this.floors = floors;
    }

    
    @Column(name="NAME", unique=true, nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="X", nullable=false)
    public int getX() {
        return this.x;
    }
    
    public void setX(int x) {
        this.x = x;
    }

    
    @Column(name="Y", nullable=false)
    public int getY() {
        return this.y;
    }
    
    public void setY(int y) {
        this.y = y;
    }




}


