/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.dao;

import com.helpdesk.micaficito.modelos.Estado;
import com.helpdesk.micaficito.util.NewHibernateUtil;
import java.util.List;
import javax.ejb.Stateless;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author programacion
 */
@Stateless
public class EstadoDaoImpl implements EstadoDao {

    @Override
    public List<Estado> listaEstado() {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        List<Estado> listaEstados=session.createQuery("From Estado").list();
        transaction.commit();
        return listaEstados;
    }

    @Override
    public void nuevoEstado(Estado estado) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.persist(estado);
        transaction.commit();
    }

    @Override
    public void actualizarEstado(Estado estado) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.saveOrUpdate(estado);
        transaction.commit();
    }

    @Override
    public Estado buscarEstado(Long idEstado) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        Estado estadoN = (Estado) session.get(Estado.class, idEstado);
        transaction.commit();
        return estadoN;

    }

    @Override
    public void eleminarEstado(Estado estado) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.delete(estado);
        transaction.commit();
    }

}
