/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.dao;

import com.helpdesk.micaficito.dto.ReporteDiarioDTO;
import com.helpdesk.micaficito.util.NewHibernateUtil;
import java.util.List;
import javax.ejb.Stateless;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Programacion
 */
@Stateless
public class ReporteDiarioDaoImpl implements ReporteDao {
    @Override
    public List<ReporteDiarioDTO> obtenerListaDiaria(String fechaInicio, String fechaFin) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<ReporteDiarioDTO> listaDiaria=null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            
            List resultWithAliasedBean = session.createSQLQuery("SELECT SUM(stda.UNITS)*-1 AS unidades, prd.NAME AS nombre, SUM(stda.PRICE) AS total FROM openbravopos.stockdiary stda "
                + "INNER JOIN openbravopos.products prd ON stda.PRODUCT=prd.ID "
                + "WHERE stda.DATENEW BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) "
                + "GROUP BY stda.PRODUCT, prd.NAME ").addScalar("unidades").addScalar("nombre").addScalar("total").setParameter(0,fechaInicio).setParameter(1,fechaFin).setResultTransformer(Transformers.aliasToBean(ReporteDiarioDTO.class)).list();
            listaDiaria=(List<ReporteDiarioDTO>)resultWithAliasedBean;
            System.out.println("tamaño de la lista ----->"+listaDiaria.size());
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        return listaDiaria;
    }

}
