/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.servicios;

import com.helpdesk.micaficito.dao.ReporteDao;
import com.helpdesk.micaficito.dto.ReporteDiarioDTO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author programacion
 */
@Stateless
public class ReporteDiarioServiceImpl implements ReporteDiarioService {
    @EJB
    ReporteDao dao;
    
    @Override
    public List<ReporteDiarioDTO> obtenerListaDiaria(String fechaInicio, String fechaFin) {
        return dao.obtenerListaDiaria(fechaInicio, fechaFin);
    }
    
}
