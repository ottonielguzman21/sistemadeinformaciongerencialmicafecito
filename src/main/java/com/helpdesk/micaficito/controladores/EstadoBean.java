package com.helpdesk.micaficito.controladores;

import com.helpdesk.micaficito.modelos.Estado;
import com.helpdesk.micaficito.servicios.EstadoService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;

@Getter
@Setter
@Named(value = "estadoBean")
@ViewScoped
public class EstadoBean implements Serializable {

    private Estado est;
    private List<Estado> todosLosEstados;
    @EJB
    private EstadoService estadoServ;

    public EstadoBean() {
    }

    @PostConstruct
    public void init() {
        est = new Estado();
        todosLosEstados = estadoServ.listaEstado();
    }

    public void guardarEstado() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (est.getId() == null) {
            estadoServ.nuevoEstado(est);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estado Guardado", "Nuevo"));
        } else {
            estadoServ.actualizarEstado(est);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Estado Actualizado", "Actualizar"));
            requestContext.execute("PF('dlg_editar_estado').hide();");
        }
    }

    public void verEstado(Estado est) {
        this.est = est;
    }

    public void eliminarEstado() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (est.getId() != null) {
            estadoServ.eleminarEstado(est);
            est = new Estado();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Estado Eliminado", "Eliminar"));
        }

    }

    public void limpiarDialogo() {
        est = new Estado();
    }
}
