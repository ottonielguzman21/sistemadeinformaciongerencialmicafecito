/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.controladores;


import com.helpdesk.micaficito.servicios.ReporteDiarioService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;
import javax.enterprise.context.SessionScoped;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Programacion
 */
@Getter
@Setter
@Named(value = "reporteDiario")
@SessionScoped
public class ReporteDiario implements Serializable {

    private StreamedContent sc;
    private Date inicio;
    private Date fin;

    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    @EJB
    private ReporteDiarioService reporte;

    public ReporteDiario() {
    }

    public JasperPrint reporteDarioProducto(Date inicio, Date fin) throws ParseException {
        JasperPrint jp = null;
        System.out.println("fecha inicio en string "+inicio);
        System.out.println("fecha fin en string "+fin);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String strInicio = dateFormat.format(inicio);
        String strFin = dateFormat.format(fin);
        System.out.println("fecha inicio en string "+strInicio);
        System.out.println("fecha fin en string "+strFin);
        SimpleDateFormat sdfProducto = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String dateInStringInicio = strInicio+" 00:00:01";
        String dateInStringFin= strFin+" 23:59:59";
        System.out.println("fecha inicio con hmss en string "+dateInStringInicio);
        System.out.println("fecha fin con hmss en string "+dateInStringFin);  
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HashMap<String, Object> props = new HashMap<String, Object>();
            JRBeanCollectionDataSource productoDataSource = new JRBeanCollectionDataSource(reporte.obtenerListaDiaria(dateInStringInicio, dateInStringFin));
            props.put("productoDataSource", productoDataSource);
            InputStream master = context.getExternalContext().getResourceAsStream("/resources/reportes/ReporteDiarioProducto.jasper");
            jp = JasperFillManager.fillReport(master, props, new JREmptyDataSource());
        } catch (JRException ex) {
            Logger.getLogger(ReporteDiario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jp;
    }

    public void generarReportePDFDiario(Date inicio, Date fin) throws ParseException {
        JasperPrint jp = reporteDarioProducto(inicio, fin);
        try {
            if (jp != null) {
                byte[] b = JasperExportManager.exportReportToPdf(jp);
                ByteArrayInputStream baos = new ByteArrayInputStream(b);
                String f = sdf.format(new Date());
                sc = new DefaultStreamedContent(baos, "application/pdf", "ReporteDiario" + "_" + f + ".pdf");
                baos.close();   
            }
        } catch (JRException ex) {
            Logger.getLogger(ReporteDiario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReporteDiario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
